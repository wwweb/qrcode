<?php

require('vendor/autoload.php');
require('system/class.google_url.php');
require('system/class.json.php');
require('system/class.config.php');
require('system/class.http.php');
require('system/class.gyazo.php');


$file = realpath('files/doge.jpg');
$response = gyazo::upload_file($file);
// print_r($response);

// $url = google_url::shorten($response['url']);
// echo $url;

// exit;

use Endroid\QrCode\QrCode;

$qrCode = new QrCode();
$qrCode
    ->setText($response['url'])
    ->setSize(500)
    ->setPadding(10)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 100))
    // ->setLabel('Hello World!')
    // ->setLabelFontSize(16)
;
ob_start();
$qrCode->render();
$raw = ob_get_clean();
$filename = md5($raw);

header('Content-Type: image/png');
header('Content-Disposition: inline; filename="'.$filename.'.png"');
echo $raw;

?>