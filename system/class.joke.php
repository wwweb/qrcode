<?php

class joke
{
    public static $sources = [
        'kickass' => 1,
        'bash' => 2
    ];

    public static $langs = ['en', 'ru'];

    public static function import()
    {
        $result['kickasshumour.com'] = self::import_kickass();
        foreach (self::$langs as $lang) {
            cache::clear('jokes_'.$lang);
        }
        return $result;
    }

    public static function import_kickass()
    {
        if (!$start = config::get('kickass_last_id')) {
            $start = -1;
        }
        $cache_file = FILES.'/cache/jokes/kickasshumor'.$start.'.html';
        if (file_exists($cache_file)) {
            $html = file_get_contents($cache_file);
        } else {
            $base_url = 'http://kickasshumor.com/ajax/alltime/5/';
            $url = $base_url.$start;
            $html = http::request($url, 'GET');
        }
        if (!$html) {
            return 'No response!';
        }
        preg_match_all('/<div class="joke.*?><\/div>/ms', $html, $match);
        if (!$match[0]) {
            return 'No jokes!';
        }
        $num = 0;
        db::prepare('insert ignore into jokes set source = :source, source_joke_id = :source_joke_id, lang = :lang, content = :content');
        $data['source'] = self::$sources['kickass'];
        $data['lang'] = 'en';
        foreach ($match[0] as $html_block) {
            if (!preg_match('/data-jokeid="(\d+)"/', $html_block, $match)) {
                continue;
            }
            $data['source_joke_id'] = $match[1];
            if (!preg_match('/<p><a.*?>(.+?)<\/a><\/p>/ms', $html_block, $match)) {
                continue;
            }
            $data['content'] = trim($match[1]);
            db::set($data);
            db::execute();
            if (db::count()) {
                $num++;
            }
        }
        // success!
        if ($num) {
            // put html content to cache file
            file_put_contents($cache_file, $html);
            // increase offset for future parsing (kickass gives 20 jokes per page)
            config::set('kickass_last_id', $start + 20);
        }
        return $num;
    }

    public static function get_random($lang = 'en')
    {
        $cache_key = 'jokes_'.$lang;
        if (!$jokes = cache::get($cache_key)) {
            db::query('select id, content from jokes where lang = ? order by rand() limit 100', $lang);
            $jokes = db::fetchAll() ?: [];
            cache::set($cache_key, $jokes);
        }
        return $jokes ? array_random($jokes) : [];
    }
}
