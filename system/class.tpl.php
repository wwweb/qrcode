<?php

defined('SYSTEM') or die('X Class Required.');

defined('SYSTEM_TEMPLATES') or define('SYSTEM_TEMPLATES', dirname(SYSTEM).DS.'templates');

class TPL {
	
	const EXTENSION = '.html';
	
	private static $templates = array();
	
	private static $blocks = array();
	
	private static $make = array();
	
	private static $tmp = array();
	
	public static $data = array();

	private static function data($key = NULL, $value = NULL, $append = false) {
		if(is_null($key))
			return self::$data;
		if(is_array($key)) {
			foreach($key as $k=>$v) self::data($k, $v);
			return self::$data;
		}
		if(is_null($value))
			return isset(self::$data[$key]) ? self::$data[$key] : false;
		if($append) {
			if(!isset(self::$data[$key]))
				self::data($key, $value);
			else self::$data[$key] .= $value;
			return self::$data[$key];
		}
		self::$data[$key] = $value;
		return $value;
	}
	
	private static function _get($key) {
		return isset(self::$data[$key]) ? self::$data[$key] : '';
	}
	
	private static function _set($key, $value) {
		self::$data[$key] = $value;
		return $value;
	}
	
	private static function _append($key, $value) {
		if(isset(self::$data[$key])) self::$data[$key] .= $value;
		else self::$data[$key] = $value;
		return self::$data[$key];
	}
	
	private static function _push($array) {
		foreach($array as $key => $value) {
			self::_set($key, $value);
		}
		return $array;
	}

	private static function _clear() {
		$args = func_get_args();
		if(empty($args))
			self::$data = array();
		else {
			foreach($args as $key)
				unset(self::$data[$key]);
		}
	}
	
	static function set($key, $val) {
		self::_set($key, $val);
	}
	
	static function get($key) {
		return self::_get($key);
	}
	
	static function append($key, $val) {
		return self::_append($key, $val);
	}
	
	static function push($array) {
		if(!is_array($array)) return false;
		return self::_push($array);
	}
	
	static function clear() {
		call_user_func_array('self::_clear', func_get_args());
	}
	
	static function reset() {
		call_user_func_array('self::_clear', self::$make);
	}
	
	// not safe
	static function walk($var, $callback) {
		if(!is_callable($callback))
			return abort('Unable to trigger callback "'.$callback.'"');
		return self::data($var, call_user_func($callback, self::data($var)));
	}
	
	/**
	*	Load template file to self::$templates
	*	Loads included files recursively
	*/
	static function load($tpl, $appendTo = NULL) {
		$tpl = strtolower($tpl);
		$file = realpath(SYSTEM_TEMPLATES . DS . $tpl . self::EXTENSION);
		if(in_array($file, self::$templates))
			return true;
		if(!$file || !is_readable($file))
			return abort('Unable to load template: '.$tpl);
		$content = file_get_contents($file);
		if(!$appendTo) $appendTo = $tpl;
		self::get_blocks($content, $appendTo);
		self::$templates[] = $file;
		return true;
	}
	
	static function load_string($content, $name) {
		self::get_blocks($content, $name);
		self::$templates[] = $name;
	}
	
	/**
	*	Make $block and append result to self::$blocks[$block] or self::$blocks[$appendTo]
	*/
	static function make($block, $appendTo = NULL) {
		if(!isset(self::$blocks[$block])) {
			// try load template file if exists
			if(!self::load($block))
				return abort('Block or template not found: '.$block);
		}
		$content = self::$blocks[$block];
		self::_make($content);
		if($appendTo) $block = $appendTo;
		if(!in_array($block, self::$make)) self::$make[] = $block;
		self::append($block, $content);
		return $content;
	}
	
	/**
	*	Make block $block and append result to $parent.
	*	When $count reached, make $parent block.
	*/
	static function imake($block, $parent, $count) {
		if($count < 1)
			return abort('self::imake() $count < 1');
		if(!isset(self::$tmp[$block]))
			self::$tmp[$block] = $count;
		self::$tmp[$block]--;
		self::make($block);
		if(self::$tmp[$block] == 0) {
			self::make($parent);
			self::clear($block);
			self::$tmp[$block] = $count;
		}
		return self::$tmp[$block];
	}
	
	/**
	*	Service method, matches and replaces variables
	*/
	protected static function _make(&$content) {
		if(preg_match_all('/{#([A-Za-z0-9_-]+)}/s', $content, $vars))
		{
			foreach($vars[1] as $key=>$varname) {
				$value = self::data($varname);
				$content = str_replace($vars[0][$key], $value, $content);
			}
		}
	}
	
	private static function get_blocks(&$content, $appendTo) {
		if(!$appendTo)
			abort('TPL :: Block name required');
		self::$blocks[$appendTo] = $content;
		if(preg_match_all('/<!--\s*([A-Za-z0-9_-]+)\s*-->(.*)<!--\s*\1\s*-->/ms', $content, $blocks)) {
			foreach($blocks[1] as $key=>$block) {
				if(isset(self::$blocks[$block])) {
					return abort('TPL :: duplicate entry for block ' . $block);
				}
				self::$blocks[$block] = $blocks[2][$key];
				// convert childs to variables
				self::$blocks[$appendTo] = str_replace($blocks[0][$key], '{#'.$block.'}', self::$blocks[$appendTo]);
				// recursion
				self::get_blocks($blocks[2][$key], $block);
			}
		}
		if(preg_match_all('/<!--\s*@([A-Za-z0-9_-]+)\s*-->/s', $content, $includes)) {
			foreach($includes[1] as $key=>$include) {
				// recursion
				self::load($include);
				// replace tag with file contents
				self::$blocks[$appendTo] = str_replace($includes[0][$key], self::$blocks[$include], self::$blocks[$appendTo]);
				// includes are not re-usable
				unset(self::$blocks[$include]);
			}
		}
	}
	
}

?>