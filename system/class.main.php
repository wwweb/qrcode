<?php

use Endroid\QrCode\QrCode;

class Main {
	
	static function get_start_img() {
		$url = config::runtime('SITE_URL');
		$filename = '_'.md5($url).'.png';
		$file = FILES.'/qr_codes/'.$filename;
		if(!file_exists($file)) {
			$qrCode = new QrCode();
			$qrCode
			->setText($url)
			->setSize(256)
			->setPadding(0)
			->setErrorCorrection('high')
			->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
			->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 127))
			->render($file);
		}
		return '/files/qr_codes/'.$filename;
	}
	
	static function generate_qr_code($text, $size = 256, $padding = 0, $correction = 'medium', $foreground = null, $background = null, $label = null, $label_size = 16, $alpha = 0) {
		if($size < 64) $size = 64;
		if($size > 512) $size = 512;
		if($padding < 0 || $padding > $size / 4) $padding = 0;
		$size -= $padding * 2;
		if(!in_array($correction, api::$error_correction)) $correction = 'medium';
		if($alpha < 0) $alpha = 0;
		if($alpha > 127) $alpha = 127;
		if(!$foreground) $foreground = ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0];
		if(!$background) $background = ['r' => 255, 'g' => 255, 'b' => 255];
		if($label_size < 8) $label_size = 8;
		if($label_size > 32) $label_size = 32;
		$background['a'] = $alpha;
		$qrCode = new QrCode();
		$qrCode
		->setText($text)
		->setSize($size)
		->setPadding($padding)
		->setErrorCorrection($correction)
		->setForegroundColor($foreground)
		->setBackgroundColor($background);
		if($label) {
			$qrCode->setLabel($label);
			$qrCode->setLabelFontSize($label_size);
		}
		else {
			$label_size = NULL;
		}
		try {
			ob_start();
			$qrCode->render();
			$content = ob_get_clean();
		}
		catch(Exception $e) {
			return false;
		}
		$md5 = md5($content);
		$file = FILES.'/qr_codes/'.$md5.'.png';
		file_put_contents($file, $content);
		db::query('replace into qr_codes set filename = :filename, file_ext = :file_ext, size = :size, padding = :padding, correction = :correction, foreground = :foreground, background = :background, content = :content, label = :label, label_size = :label_size',
		[
			'filename' => $md5,
			'file_ext' => 'png',
			'size' => $size,
			'padding' => $padding,
			'correction' => $correction,
			'foreground' => json::encode($foreground),
			'background' => json::encode($background),
			'content' => $text,
			'label' => $label ?: NULL,
			'label_size' => $label_size
		]);
		return $md5.'.png';
	}
	
	static function layout() {
		if(!$document_meta = config::get('document_meta')) {
			$document_meta = [
				'description' => 'Free online QR Code generator to make your own QR Codes',
				'keywords' => 'QR Code, QRCode, qr code generator online, generate qr code, create qr code',
				'title' => 'QR Code generator online. Create a QR Code'
			];
			config::set('document_meta', $document_meta);
		}
		if(!tpl::get('document_meta_description')) tpl::set('document_meta_description', $document_meta['description']);
		if(!tpl::get('document_meta_keywords')) tpl::set('document_meta_keywords', $document_meta['keywords']);
		if(!tpl::get('document_title')) tpl::set('document_title', $document_meta['title']);
		echo tpl::make('main-layout');
	}
	
}
tpl::load('main-layout');

?>