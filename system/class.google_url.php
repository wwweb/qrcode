<?php

class google_url {
	
	const API_KEY = 'AIzaSyCN-KpYt4MytWxAyon5-uG5PT-nrEescGw';
	
	static function shorten($url) {
		$client = new Google_Client();
		$client->setDeveloperKey(self::API_KEY);
		$service = new Google_Service_Urlshortener($client);
		$url_model = new Google_Service_Urlshortener_Url();
		$url_model->setLongUrl($url);
		$response = $service->url->insert($url_model);
		return $response['id'];
	}
	
}

?>