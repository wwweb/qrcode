<?php

class Str {

	static function starts($needle, $haystack, $case=false) {
		return ($case)
			? strpos($haystack, $needle, 0) === 0
			: stripos($haystack, $needle, 0) === 0;
	}

	static function ends($needle, $haystack, $case=false) {
		$expectedPosition = strlen($haystack) - strlen($needle);
		return ($case)
			? strrpos($haystack, $needle, 0) === $expectedPosition
			: strripos($haystack, $needle, 0) === $expectedPosition;
	}
	
	static function numberof($numberof, $value, $suffix) {
		$keys = array(2, 0, 1, 1, 1, 2);
		$mod = $numberof % 100;
		$suffix_key = $mod > 4 && $mod < 20 ? 2 : $keys[min($mod%10, 5)];
		return $value . $suffix[$suffix_key];
	}
	
	static function rus2translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => "'",  'ы' => 'y',   'ъ' => "'",
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => "'",  'Ы' => 'Y',   'Ъ' => "'",
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}
	
	static function utf8($str) {
		return mb_convert_encoding($str, 'UTF-8');
	}
	
	static function str2url($str) {
		$str = str::rus2translit($str);
		$str = mb_strtolower($str);
		$str = preg_replace('#[^\w ]+#', '', $str);
		$str = preg_replace('#\s+#', '-', $str);
		return $str;
	}
	
	static function generatePassword($lenght = 0) {
		if(!$length) $length = rand(8, 11);
		return substr(md5(microtime()), 0, $length);
	}
	
	static function valid_email($email) {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	
	static function xmltoarray($xml) {
		$xml = simplexml_load_string($xml);
		$xml = json::encode($xml, JSON_UNESCAPED_UNICODE);
		$array = json::decode($xml);
		return $array;
	}
	
	static function file_to_utf8($file) {
		$content = file_get_contents($file);
		if(!mb_check_encoding($content, 'UTF-8')) {
			$content = mb_convert_encoding($content, 'UTF-8');
			file_put_contents($file, '\xEF\xBB\xBF'.$content);
		}
		return $content;
	}

}

?>