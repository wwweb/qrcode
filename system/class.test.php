<?php

set_time_limit(300);

class Test {
	
	static function run($path, $method) {
		if(!account::$admin && REQUEST('key') !== '5b2f22faa2ccb1c76c512eab6c12a1328d824de2') die('ACCESS DENIED');
		if(!method_exists('Test', $method)) {
			die('NO SUCH METHOD');
		}
		call_user_func('Test::'.$method);
	}
	
	static function globals() {
		ob_start();
		echo '<pre style="background:white">';
		print_r($GLOBALS);
		printf("Memory: %d\r\n", memory_get_usage());
		print_r(cache::stats());
		print_r(db::$queries);
		db::query('show profiles');
		print_r(db::fetchAll());
		echo '</pre>';
		$out = ob_get_clean();
		if(qs::$params['path'] == '/test/globals') tpl::append('main', $out);
		else echo $out;
	}
	
	static function phpinfo() {
		phpinfo();
		exit;
	}
	
}

?>