<?php

function status($statusCode = 200)
{
    header('HTTP/1.0: '.$statusCode, true);
}

function fileval($file)
{
    $file = realpath($file);
    if (!$file) {
        return abort('File not found: ' . $file);
    }
    ob_start();
    include($file);
    return ob_get_clean();
}

function absint($var)
{
    return abs(intval($var));
}

function finfo($file, $key=false)
{
    $finfo = pathinfo($file);
    return $key ? $finfo[$key] : $finfo;
}

function fileext($filename)
{
    if (!is_file($filename)) {
        $f = explode('.', $filename);
        return end($f);
    }
    return finfo($filename, 'extension');
}

function redirect($url = null, $status = 302)
{
    if (!$url) {
        $url = SERVER('REQUEST_URI') ?: '/';
    }
    header('HTTP/1.0 '.$status, true);
    header('Location: '.$url, true);
    exit;
}

function array_random(&$array)
{
    $key = array_rand($array);
    return $key === false ? false : $array[$key];
}

function array_group(array $array, $key, $index = null)
{
    $result = array();
    foreach ($array as $k=>$v) {
        if (is_null($index)) {
            $result[$v[$key]][] = $v;
        } else {
            $result[$v[$key]][$v[$index]] = $v;
        }
    }
    return $result;
}

function html2chars($str)
{
    $replacement = get_html_translation_table(HTML_SPECIALCHARS);
    return strtr($str, $replacement);
}

function chars2html($str)
{
    $replacement = array_flip(get_html_translation_table(HTML_SPECIALCHARS));
    return strtr($str, $replacement);
}

// debug_print_backtrace()
function abort($errormsg=null)
{
    ob_start();
    if ($errormsg) {
        echo '<h3>'.$errormsg."</h3>\n";
    }
    echo "<pre>\n";
    debug_print_backtrace();
    echo "</pre>\n";
    header('Content-Type:text/html;charset=utf-8', true);
    print(ob_get_clean());
    if (config::runtime('DEBUG')) {
        die();
    }
}

function get_files($dir='./', $pattern='/\w+/', $skip=0, $limit=0)
{
    // correct directory path
    if (is_file($dir)) {
        $dir = dirname($dir);
    }
    $dir = strtr($dir, '\\', '/');
    if (substr($dir, -1)!=='/') {
        $dir .= '/';
    }
    if (!$d = opendir($dir)) {
        abort('unable to open dir: '.$dir);
    }
    $result = array();
    while (false!==$file=readdir($d)) {
        if (!is_file($dir.$file)) {
            continue;
        }
        if ($skip>0) {
            $skip--;
            continue;
        }
        if (preg_match($pattern, $file)) {
            $result[] = $dir.$file;
            $limit--;
            if ($limit==0) {
                break;
            }
        }
    }
    closedir($d);
    return $result;
}

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row) {
                $tmp[$key] = $row[$field];
            }
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function array_closest_num($num, $arr)
{
    $closest = null;
    foreach ($arr as $n) {
        if ($closest === null || abs($num - $closest) > abs($n - $num)) {
            $closest = $n;
        }
    }
    return $closest;
}

function upload_file($file, $dest_dir, $filename = null)
{
    $file['name'] = mb_convert_encoding($file['name'], 'UTF-8');
    $dest = $dest_dir . ($filename ? $filename : $file['name']);
    return (move_uploaded_file($file['tmp_name'], $dest)) ? $dest : false;
}

function ts2date($format, $timeStr)
{
    return date($format, strtotime($timeStr));
}

function GET($key)
{
    return isset($_GET[$key]) ? $_GET[$key] : '';
}
function POST($key)
{
    return isset($_POST[$key]) ? $_POST[$key] : '';
}
function REQUEST($key)
{
    return isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
}
function COOKIE($key)
{
    return isset($_COOKIE[$key]) ? $_COOKIE[$key] : '';
}
function SESSION($key)
{
    return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
}
function SERVER($key)
{
    return isset($_SERVER[$key]) ? $_SERVER[$key] : '';
}

function check_sid($sid=null)
{
    if ($sid==null && isset($_REQUEST['sid']) && !empty($_REQUEST['sid'])) {
        $sid = $_REQUEST['sid'];
    }
    return empty($sid) ? false : $sid == session_id();
}

function check_captcha($code)
{
    $code = trim($code);
    $ret = (!empty($code) && strtolower($code)==strtolower(SESSION('captcha')));
    $_SESSION['captcha'] = null;
    return $ret;
}

function alert($msg, $class='notice', $appendTo = 'main')
{
    site::data('alert-msg', $msg);
    site::data('alert-class', $class);
    return tpl::make('alert', $appendTo);
}

function offset($limit = 20)
{
    return ($page = absint(GET('p'))) > 1 ? ($page - 1) * $limit : 0;
}

function make_title($title_text, $appendTo = 'main')
{
    tpl::set('document-title', $title_text);
    tpl::make('page-title', $appendTo);
}

function correct_float($val)
{
    $val = str_replace(',', '.', $val);
    $val = preg_replace('/([^\d\.])/s', '', $val);
    return floatval($val);
}

function moneyFormat($float, $decimals = 0)
{
    $float = ceil($float);
    return number_format($float, $decimals, '.', ' ');
}

function discountPrice($price, $discount)
{
    return ceil($price * (100 - $discount) / 100);
}

function set_cookie($key, $value)
{
    if (is_null($value)) {
        setcookie($key, null, time() - config::runtime('COOKIE_LIFETIME'), '/', config::runtime('COOKIE_DOMAIN'));
    } else {
        setcookie($key, $value, time() + config::runtime('COOKIE_LIFETIME'), '/', config::runtime('COOKIE_DOMAIN'));
    }
}

function log_write($str)
{
    $date = date('Y-m-d H:i:s');
    file_put_contents(FILES.'/x.log', $date.' '.$str."\r\n", FILE_APPEND);
}

function die_stupid_bot()
{
    if (REQUEST('leaveempty') || REQUEST('empty') || REQUEST('robot') || REQUEST('bot')) {
        status('401 Access Denied');
        die('DIE STUPID BOT! >:D');
    }
}
function float_rand($min, $max, $round = 1)
{
    $randomfloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
    if ($round>0) {
        $randomfloat = round($randomfloat, $round);
    }
    return $randomfloat;
}
function add_pc($number, $pc)
{
    return $number * (1 + $pc / 100);
}

if (!function_exists('password_hash')) {
    function password_hash($password)
    {
        return sha1($password);
    }
}

function br2nl($str, $nl = "\r\n")
{
    $str = preg_replace('#<br.*?>\s*#i', $nl, $str);
    return $str;
}

function ceilby($num, $by)
{
    return ceil($num / $by) * $by;
}

function floorby($num, $by)
{
    return floor($num / $by) * $by;
}

function hex2rgb($hexStr, $returnAsString = false, $seperator = ',')
{
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = [];
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
        $colorVal = hexdec($hexStr);
        $rgbArray['r'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['g'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['b'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
        $rgbArray['r'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
        $rgbArray['g'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
        $rgbArray['b'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        return false; //Invalid hex color code
    }
    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray;
}
