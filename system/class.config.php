<?php

class config
{
    public const CACHE_id = 'site_config';
    public const CACHE_LIFETIME = 10e4;
    public static $RUNTIME = []; // runtime config

    public static function set($id, $value)
    {
        $data['id'] = $id;
        if (is_array($value) || is_object($value) || $value instanceof Traversable) {
            $data['value'] = json::encode($value);
            $data['json'] = 1;
        } else {
            $data['value'] = $value;
            $data['json'] = 0;
        }
        db::query('replace into config set `id` = :id, value = :value, json = :json', $data);
        self::reload();
        return $value;
    }

    public static function get($id = null)
    {
        if (!$config = cache::get(self::CACHE_id)) {
            $config = self::reload();
        }
        if (!$id) {
            return $config;
        }
        if (!isset($config[$id])) {
            return null;
        }
        return $config[$id];
    }

    public static function reload()
    {
        $config = [];
        db::query('select * from config order by `id`');
        while ($row = db::fetchArray()) {
            if ($row['json']) {
                $value = json::decode($row['value']);
            } else {
                $value = $row['value'];
            }
            $config[$row['id']] = $value;
        }
        cache::set(self::CACHE_id, $config, self::CACHE_LIFETIME);
        return $config;
    }

    public static function runtime($id, $value = null)
    {
        if (is_null($id)) {
            return self::$RUNTIME;
        }
        if (is_null($value)) {
            return isset(self::$RUNTIME[$id]) ? self::$RUNTIME[$id] : null;
        }
        self::$RUNTIME[$id] = $value;
        return $value;
    }
}
