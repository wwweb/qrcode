<?php

class Gyazo {
	
	const POST = 'https://upload.gyazo.com/api/upload';
	const GET = 'https://api.gyazo.com/api/images';
	const CLIENT_ID = 'ca5fb45a9118e0d976b3935feb3ebb702e4146c42ecfcf7bd721fd2b8abd810b';
	const CLIENT_SECRET = '69c06be364166ad54099783f7fa7200a31c4aa7f8b5ead4d2969b55fb58c474c';
	const ACCESS_TOKEN = 'b992adfd21f6e26798db6dc60c53746c5d784faaa933a71916e0909fdb5842e1';
	
	static function upload_file($file) {
		if(!self::check_file($file)) return ['error'=>'file check fail'];
		$curl_file = new CURLFile($file);
		$data = [
			'imagedata' => $curl_file
		];
		$response = self::request(self::POST, $data);
		return json::decode($response);
	}
	
	static function get_list($page = 1, $per_page = 20) {
		$data = [
			'page' => $page,
			'per_page' => $per_page
		];
		$response = self::request(self::GET, $data);
		return json::decode($response);
	}
	
	static function request($url, $data = []) {
		$data['access_token'] = self::ACCESS_TOKEN;
		$method = $url == self::POST ? 'POST' : 'GET';
		$response = http::request($url, $method, $data);
		return $response;
	}
	
	static function check_file($file) {
		if(!file_exists($file) || !is_readable($file)) return false;
		$allowed_types = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'];
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime_type = finfo_file($finfo, $file);
		finfo_close($finfo);
		if(!in_array($mime_type, $allowed_types)) return false;
		return true;
	}
	
}

?>