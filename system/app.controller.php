<?php

// значение по умолчанию для первого визита
if(!isset($_SESSION['referer'])) $_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'];

// --- ROUTES ---

site::route('/', 'action.index.php');
site::route('/test/(\w+)', 'test::run');
site::route('/cron/(\w+)', 'cron::run');
site::route('/ajax/(\w+)', 'ajax::request');
site::route('/api/(\w+)', 'api::call');
site::route('/public_api', 'api::view_docs');

// --- RUN IT! ---

if(substr($_SERVER['REQUEST_URI'],0,6) == '/admin') {
	site::run('admin::layout');
}
else {
	site::run('main::layout');
}

// откуда пришел клиент (для следующего запроса)
$_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

session_write_close();

if(config::runtime('DEBUG')) {
	$GLOBALS['process_time'] = sprintf('%01.4f', microtime(true) - $start_microtime);
	test::globals();
}

?>