<?php

class DB {
	
	private static $connection = NULL; // PDO Object
	private static $st = NULL;			// PDO Statement
	public static $queries = array(); // prepared queries list
	
	static function checkStatement() {
		if(!db::$st instanceof PDOStatement)
			abort('DB :: Not a statement');
	}
	
	static function connect($config = NULL, $return_pdo = false) {
		if(!is_array($config)) $config = config::runtime('DB');
		if(is_file($config['host'])) {
			$dsn = 'sqlite:'.realpath($config['host']);
		}
		else {
			$dsn = 'mysql:host='.$config['host'];
			if(is_string($config['dbname']) && !empty($config['dbname']))
				$dsn .= ';dbname='.$config['dbname'];
			if(isset($config['port']))
				$dsn .= ';port='.$config['port'];
			$dsn .= ';charset=utf8';
		}
		try {
			if(!isset($config['options'])) {
				$config['options'] = [];
			}
			if($connection = new PDO($dsn, $config['user'], $config['password'], $config['options'])) {
				$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
				$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
				if($return_pdo) return $connection;
				db::$connection = $connection;
			}
		}
		catch(PDOException $e) {
			abort($e->getMessage());
		}
	}
	
	static function attr() {
		$args = func_get_args();
		return call_user_func_array(array(db::$connection, 'setAttribute'), $args);
	}
	
	static function createFunction($fname, $callback) {
		return db::$connection->sqliteCreateFunction($fname, $callback);
	}
	
	static function begin() {
		return db::$connection->beginTransaction();
	}
	
	static function commit() {
		return db::$connection->commit();
	}
	
	static function rollback() {
		return db::$connection->rollBack();
	}
	
	static function escape($str, $param=PDO::PARAM_STR) {
		return db::$connection->quote($str, $param);
	}
	
	/**
	*	prepare sql
	*	bind parameters passed after sql (array/object|param,val|param,val,type)
	*/
	static function prepare($sql) {
		if(!db::$st = db::$connection->prepare($sql))
			abort('DB :: Unable to prepare a statement.');
		db::$queries[] = db::$st->queryString;
		$params = array_slice(func_get_args(), 1);
		if(!empty($params)) {
			db::set($params);
		}
	}
	
	/**
	*	extended bindValue() method
	*	1 arg: array(key1=>val1, key2=>val2), others
	*	2 args: key, value
	*	3 args: key, value and type
	*/
	static function set($param, $value = NULL, $type = NULL) {
		db::checkStatement();
		if(is_array($param)) {
			foreach($param as $key=>$value) {
				if(is_array($value))
					return db::set($value);
				if(is_integer($key)) $key += 1; // non-assoc array
				db::set($key, $value);
			}
			return;
		}
		if(is_null($type)) $type = db::datatype($value);
		db::$st->bindValue($param, $value, $type);
	}
	
	static function execute() {
		if(!db::$st->execute()) {
			// var_dump(self::$queries);
			print_r(self::$st);
			db::$st->debugDumpParams();
			abort(db::error());
			return false;
		}
		return true;
	}
	
	/**
	*	queryString is a readonly parameter of PDOStatement object
	*/
	static function queryString() {
		return db::$st->queryString;
	}
	
	/**
	*	prepare and execute sql
	*	bind parameters passed after sql (array/object|param,val|param,val,type)
	*/
	static function query($sql) {
		call_user_func_array('db::prepare', func_get_args());
		return db::$st->execute();
	}
	
	/**
	*	query and fetch single column|row
	*/
	static function querySingle($sql, $entire_row=false) {
		db::query($sql);
		return $entire_row
			? db::$st->fetch(PDO::FETCH_ASSOC)
			: db::$st->fetchColumn();
	}
	
	static function lastInsertId() {
		return db::$connection->lastInsertId();
	}
	
	static function fetchSingle($index = 0) {
		db::checkStatement();
		return db::$st->fetchColumn($index);
	}
	
	static function fetchArray($mod = PDO::FETCH_ASSOC) {
		db::checkStatement();
		return db::$st->fetch($mod);
	}
	
	static function fetchAll($mod = PDO::FETCH_ASSOC) {
		db::checkStatement();
		return db::$st->fetchAll($mod);
	}
	
	static function count() {
		db::checkStatement();
		return db::$st->rowCount();
	}
	
	/**
	*	get data type of a value
	*/
	static function datatype(&$value) {
		if(is_string($value)) return PDO::PARAM_STR;
		if(is_numeric($value)) return PDO::PARAM_INT;
		if(is_bool($value)) return PDO::PARAM_BOOL;
		if(is_null($value)) return PDO::PARAM_NULL;
		abort('DB :: Unsupported data type');
	}
	
	static function error($as_array = false) {
		return $as_array ? db::$st->errorInfo() : implode('; ', db::$st->errorInfo());
	}
	
}

db::connect();

?>