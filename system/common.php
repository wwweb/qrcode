<?php

session_name('session_id');
session_start();

if(!headers_sent()) {
	header('Content-Type:text/html;charset=utf-8');
	header('X-UA-Compatible: IE=Edge');
}

define('CURRENT_PAGE', (($p = absint(GET('p'))) > 1) ? $p : 1);

tpl::load('misc');
tpl::load('common');
tpl::make('loadgif');
tpl::set('document-title', config::runtime('DOCUMENT_TITLE'));

?>