<?php

class DBCache
{
    public function __construct()
    {
        // do nothing
    }

    public function connect()
    {
        return true;
    }

    private static function gc()
    {
        if (mt_rand(0, 49)) {
            return;
        } // 2% probability
        db::query('delete from cache where expires > unix_timestamp()');
    }

    public function get($id)
    {
        db::query('select * from cache where `id` = ?', $id);
        if (!$row = db::fetchArray()) {
            return false;
        }
        if (time() > $row['expires']) {
            return false;
        }
        return unserialize($row['value']);
    }

    public function set($id, $value, $lifetime)
    {
        $value = serialize($value);
        $expires = time() + $lifetime;
        db::query('replace into cache set `id` = :id, `value` = :value, expires = :expires', [
            'id' => $id,
            'value' => $value,
            'expires' => $expires
        ]);
        $result = (bool) db::count();
        self::gc();
        return $result;
    }

    public function delete($id)
    {
        db::query('delete from cache where `id` = ?', $id);
        return true;
    }

    public function flush()
    {
        db::query('truncate table cache');
        return true;
    }

    public function getStats()
    {
        $result = [];
        db::query('select * from cache order by expires desc');
        while ($row = db::fetchArray()) {
            $row['value'] = unserialize($row['value']);
            $row['expires_datetime'] = date('Y-m-d H:i:s', $row['expires']);
            $result[] = $row;
        }
        return $result;
    }
}
