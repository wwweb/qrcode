<?php

if(!$start_img = SESSION('start_img')) {
	$start_img = main::get_start_img();
	$_SESSION['start_img'] = $start_img;
}

tpl::set('qrcode-src', $start_img);
tpl::set('qrcode-alt', $start_img);

$levels = api::$error_correction;
sort($levels);
foreach($levels as $level) {
	tpl::set('option-value', $level);
	tpl::set('option-text', $level);
	tpl::make('option', 'option-correction');
}

tpl::make('index', 'main');

?>