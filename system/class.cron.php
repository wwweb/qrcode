<?php

class Cron {
	
	static function run($path, $method, $return_result = false) {
		$method = strtolower($method);
		if(!method_exists(__class__, $method)) die('NO SUCH METHOD');
		$result = call_user_func(__class__ .'::'.$method);
		if($return_result) return $result;
		$output = ['cron::'.$method => $result];
		print("<pre>\n");
		print_r($output);
		print("\n</pre>");
		exit;
	}
	
	static function gc() {
		$now = time();
		$num = 0;
		$d = dir(FILES.'/qr_codes/');
		while($filename = $d->read()) {
			if(preg_match('/^\w{32}\.png$/', $filename)) {
				$file = FILES.'/qr_codes/'.$filename;
				if($now - filemtime($file) > 86400 * 7) {
					unlink($file);
					$num++;
				}
			}
		}
		$d->close();
		return $num;
	}
	
	static function import_jokes() {
		return joke::import();
	}
	
}

?>