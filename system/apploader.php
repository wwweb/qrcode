<?php

ini_set('mbstring.func_overload', 0);
error_reporting(-1);
setlocale(LC_TIME, 'ru_RU.UTF-8', 'rus');

define('AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('REQUEST_POST', REQUEST_METHOD == 'POST');
define('REQUEST_GET', REQUEST_METHOD == 'GET');

require(SYSTEM.DS.'class.site.php');
require(SYSTEM.DS.'functions.php');
require(SYSTEM.DS.'common.php');
require(VENDOR.DS.'autoload.php');

require_once(SYSTEM.'/config.'.SERVER('SERVER_NAME').'.php');

require(SYSTEM.'/app.controller.php');

?>