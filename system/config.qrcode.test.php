<?php

config::runtime('DB', array(
    'host' => 'localhost',
    'user' => 'root',
    'password' => '',
    'dbname' => 'qrcode',
    // 'options'=> array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET profiling=1;'),
    'port' => 3306
));

config::runtime('MEMCACHE', array(
    'host' => 'localhost',
    'port' => 11211
));

config::runtime('COOKIE_LIFETIME', 86400 * 30);
config::runtime('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);

config::runtime('SITE_URL', 'http://'.$_SERVER['HTTP_HOST']);

config::runtime('DEBUG', true);
