<?php

class HTTP {

	static function request($url, $method = 'GET', $data = NULL, $options = NULL, $debug = false) {
		$curl = curl_init();
		$opts = array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_CONNECTTIMEOUT => 30,
			CURLOPT_URL => $url,
			CURLINFO_HEADER_OUT => TRUE,
			CURLOPT_SSL_VERIFYPEER => FALSE
		);
		if(is_array($options)) {
			$opts += $options;
		}
		if($method == 'POST') {
			$opts[CURLOPT_POST] = TRUE;
			if((is_array($data) && !empty($data)) || is_string($data)) {
				$opts[CURLOPT_POSTFIELDS] = $data;
			}
		}
		elseif($method == 'GET' && $data) {
			$querystring = http_build_query($data);
			$opts[CURLOPT_URL] .= '?'.$querystring;
		}
		curl_setopt_array($curl, $opts);
		$response = curl_exec($curl);
		if($debug) print_r(curl_getinfo($curl));
		if(config::runtime('DEBUG')) $GLOBALS['CURL_INFO'] = curl_getinfo($curl);
		$error = curl_error($curl);
		curl_close($curl);
		return $error ? $error : $response;
	}

}

?>