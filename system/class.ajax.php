<?php

class Ajax {
	
	static function request($path, $method) {
		$method = strtolower($method);
		if(!method_exists(__class__, $method)) die(json::encode(['error'=> 'method not found']));
		$args = func_get_args();
		$args = array_slice($args, 2);
		$result = call_user_func_array(__class__ .'::'.$method, $args);
		header('Content-Type: application/json');
		die(json::encode($result));
	}
	
	static function tell_me_a_joke() {
		$lang = trim(GET('lang'));
		if(!in_array($lang, joke::$langs)) $lang = 'en';
		$joke = joke::get_random($lang);
		if(!$joke) return ['error' => 'Sorry! Something went wrong. No jokes for now! :('];
		if(strpos($joke['content'], '<br')) $joke['content'] = br2nl($joke['content'], "\n");
		$joke['content'] .= "\n\n Source: kickasshumor.com\n";
		return ['error' => null, 'joke' => $joke];
	}
	
}

?>