<?php

class api {
	
	const ERROR_UNACCEPTABLE = 'Unable to generate qr code. Parameters unacceptable.';
	const IMG_UNACCEPTABLE = 'unacceptable.png';
	const ERROR_NOTEXT = 'No content provided.';
	const IMG_NOTEXT = 'notext.jpg';
	const ERROR_SAMECOLOR = 'Background and foreground colors must be different.';
	const IMG_SAMECOLOR = 'samecolor.jpg';
	
	static $error_correction = ['low','medium','quartile','high'];
	
	static function call($path, $method) {
		$method = strtolower($method);
		if(!method_exists(__class__, $method)) die('error: method "'. $method .'" does not exist');
		$args = func_get_args();
		$args = array_slice($args, 2);
		$result = call_user_func_array(__class__ .'::'.$method, $args);
		header('Content-Type: application/json');
		die(json::encode($result));
	}
	
	static function generate() {
		$response = ['error' => null, 'img' => self::IMG_UNACCEPTABLE];
		$text = trim(REQUEST('text'));
		$label = trim(REQUEST('label'));
		$size = intval(REQUEST('size'));
		$padding = intval(REQUEST('padding'));
		$alpha = intval(REQUEST('alpha'));
		$correction = trim(REQUEST('correction'));
		$background = hex2rgb(REQUEST('background'));
		$foreground = hex2rgb(REQUEST('foreground'));
		$label_size = intval(REQUEST('label_size'));
		if(!$text) {
			$response['error'] = self::ERROR_NOTEXT;
			$response['img'] = self::IMG_NOTEXT;
		}
		elseif($background == $foreground && !$alpha) {
			$response['error'] = self::ERROR_SAMECOLOR;
			$response['img'] = self::IMG_SAMECOLOR;
		}
		elseif(!$img = main::generate_qr_code($text, $size, $padding, $correction, $foreground, $background, $label, $label_size, $alpha)) {
			$response['error'] = self::ERROR_UNACCEPTABLE;
			$response['img'] = self::IMG_UNACCEPTABLE;
		}
		else {
			$response['img'] = $img;
			$_SESSION['start_img'] = '/files/qr_codes/'.$img;
		}
		return $response;
	}

	static function view_docs() {
		tpl::set('document_title', 'Public API - QR Code Generator online');
		tpl::make('apidoc', 'main');
	}
	
}

?>