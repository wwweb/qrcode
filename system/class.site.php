<?php

class Site {

	private static $routes = array();

	/**
	*	ROUTER
	*	'GET/' and 'POST/' patterns are allowed
	*/
	static function route($pattern, $action) {
		// if(!preg_match('/(GET|POST|REQUEST)(\/.*)/', $pattern, $match))
			// return abort('X :: Syntax error in route pattern: '.$pattern);
		site::$routes[$pattern] = $action;
	}

	static function run($callback = NULL) {

		$CALL = false;
		$PATH = qs::$params['path'];
		$ALTPATH = substr($PATH, -1) == '/' ? substr($PATH, 0, strlen($PATH) - 1) : $PATH.'/';

		foreach(site::$routes as $pattern => $action) {
			if(preg_match('#^'.$pattern.'$#u', $PATH, $match)) {
				site::do_action($action, $match);
				$CALL = TRUE;
				break;
			}
			elseif(preg_match('#^'.$pattern.'$#ui', $ALTPATH, $match)) {
				return redirect($ALTPATH, 301);
			}
		}

		//default action
		if(!$CALL) {
			site::do_action('action.404.php');
		}

		if(is_callable($callback)) {
			site::do_action($callback);
		}
	}

	static function do_action($action, $args = array()) {
		// action is file to include
		if(is_string($action) && preg_match('/\.php$/', $action)) {
			$file = SYSTEM.DS.$action;
			if(!file_exists($file) || !is_readable($file))
				return abort('Site :: file not found: ' . $file);
			include($file);
		}
		// action is callable
		else {
			if(!is_callable($action))
				return abort('Site :: Unable to trigger action: ' . $action);
			if(empty($args)) {
				$path_parts = explode('/', qs::$params['path']);
				$args = (array) end($path_parts);
			}
			call_user_func_array($action, $args);
		}
	}

	static function autoloader($class) {
		$class = strtolower($class);
		$filename = SYSTEM.DS.'class.'.$class.'.php';
		if(!file_exists($filename)) $filename = str_replace('class.', '', $filename);
		if(!file_exists($filename)) abort('unable to load class "'.$class.'"');
		include_once($filename);
	}
}
spl_autoload_register('site::autoloader');

?>