<?php

class Cache {
	
	private static $connection = NULL;
	
	static function init() {
		if(!self::$connection) {
			$er = error_reporting();
			error_reporting(0);
			$config = config::runtime('MEMCACHE');
			self::$connection = class_exists('Memcache', false) ? new Memcache : new DBCache();
			if(!self::$connection->connect($config['host'], $config['port']))
				self::$connection = NULL;
			error_reporting($er);
		}
	}
	
	static function get($key) {
		if(!self::$connection) return false;
		return self::$connection->get($key);
	}
	
	static function set($key, $value, $lifetime = 3600) {
		if(!self::$connection) return false;
		return self::$connection->set($key, $value, 0, $lifetime);
	}
	
	static function clear($key) {
		if(!self::$connection) return false;
		if(is_array($key)) {
			foreach($key as $k) self::clear($k);
		}
		return self::$connection->delete($key);
	}
	
	static function flush() {
		if(!self::$connection) return false;
		return self::$connection->flush();
	}
	
	static function stats() {
		if(!self::$connection) return false;
		return self::$connection->getStats();
	}
	
}

cache::init();

?>
