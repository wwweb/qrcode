<?php

$GLOBALS['start_microtime'] = microtime(true);

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('VENDOR', ROOT.DS.'vendor');
define('SYSTEM', ROOT.DS.'system');
define('FILES', ROOT.DS.'files');

require(SYSTEM.DS.'apploader.php');

?>